#include "mem.h"
#include <stdio.h>
#define HEAP_START_SIZE 0
#define ALLOC_MEM_SIZE 8175

struct student {
    char* name;
    int64_t progress;
    bool is_sleep;
};

int main() {
    debug("Tests started!\n");

    void* heap = heap_init(HEAP_START_SIZE);
    debug_heap(stdout, heap);

    debug("Test 1: successful memory allocation\n");
    struct student* student_1 = _malloc(sizeof(struct student));
    student_1->name = "Kostya";
    student_1->progress = 103;
    student_1->is_sleep = true;
    printf("Happy %s with %ld points in programming languages sleep: %d\n", student_1->name, student_1->progress, student_1->is_sleep);
    debug_heap(stdout, heap);

    debug("Test 2: free one block\n");
    struct student* student_2 = _malloc(sizeof(struct student));
    student_2->name = "Vasya";
    student_2->progress = 0;
    student_2->is_sleep = false;
    struct student* student_3 = _malloc(sizeof(struct student));
    student_3->name = "Kolya";
    student_3->progress = 50;
    student_3->is_sleep = false;
    debug_heap(stdout, heap);
    debug("free second block\n");
    _free(student_2);
    debug_heap(stdout, heap);
    
    debug("Test 3: free two blocks\n");
    struct student* student_4 = _malloc(sizeof(struct student));
    student_4->name = "Vasya";
    student_4->progress = 0;
    student_4->is_sleep = false;
    debug_heap(stdout, heap);
    debug("free first and third block\n");
    _free(student_1);
    _free(student_3);
    debug_heap(stdout, heap);

    debug("Test 4: expansion memory after last region\n");
    debug_heap(stdout, heap);
    _malloc(ALLOC_MEM_SIZE + 24);
    debug_heap(stdout, heap);
    heap_term();

    debug("Test 5: expansion memory in other region\n");
    heap = heap_init(HEAP_START_SIZE);
    void* new = mmap(heap + ALLOC_MEM_SIZE + 10, ALLOC_MEM_SIZE * 2, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0);
    _malloc(ALLOC_MEM_SIZE + 100);
    debug_heap(stdout, heap);
    _free(new);
    heap_term();
    return 0;
}